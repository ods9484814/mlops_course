FROM nvidia/cuda:12.3.2-base-ubuntu22.04

RUN apt-get update && apt-get install -y wget && \
    rm -rf /var/lib/apt/lists/*

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /miniconda.sh && \
    bash /miniconda.sh -b -u -p /miniconda && \
    rm /miniconda.sh
ENV PATH="/miniconda/bin:${PATH}"

RUN conda install -c conda-forge mamba

WORKDIR /mlops_course

COPY pyproject.toml /mlops_course/pyproject.toml
COPY environment.yml /mlops_course/environment.yml
RUN mamba env update -n base -f /mlops_course/environment.yml && \
    mamba clean -afy

CMD ["/bin/bash"]
