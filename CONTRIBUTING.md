# Contributing to MLOps_course
This document provides guidelines and instructions to help you set up your development environment and ensure your contributions meet our standards.

## Setting Up Your Development Environment
We use `Miniconda` to manage dependencies and ensure consistency across development environments. Follow these steps to set up your environment:

### 1. Install Miniconda

If you haven't already, download and install Miniconda from [Miniconda's official site](https://docs.conda.io/en/latest/miniconda.html).

### 2. Clone the Repository

Clone the repository:

```bash
git clone https://gitlab.com/ods9484814/mlops_course.git
cd mlops_course
```

### 3. Create Your Development Environment
We've provided an `environment-dev.yml` file that specifies all the necessary development dependencies. Create your Conda environment by running:

```bash
conda env create -f environment-dev.yml
```
This command creates a new Conda environment named mlops_course_dev and installs all specified dependencies.

### 4. Activate Your Environment
Activate the newly created environment:
```bash
conda activate mlops_course_dev
```

## Using Pre-commit
We use pre-commit hooks to ensure code quality and consistency. These hooks run checks like code formatting and linting before you commit changes.

### 1. Set Up Pre-commit in Local Repository
Set up pre-commit (pre-commit should already be installed as part of your dev environment setup):
```bash
pre-commit install
```

### 2. Run Pre-commit Manually (Optional)
If you want to manually run all pre-commit hooks on all files in the repository, execute:
```bash
pre-commit run --all-files
```


## Code Style Guidelines
You can integrate [Black](https://github.com/psf/black) and [Ruff](https://github.com/charliermarsh/ruff) with Visual Studio Code to format your code automatically. For setup instructions, refer to [this video](https://www.youtube.com/watch?v=eBaUoN2ExHU).

Also, you can run these tools manually, just run:
```bash
black path/to/file.py
ruff path/to/file.py
```

## (Optional) Docker Development Environment

This section provides instructions on how to build and run the development Docker image for the project.

### Building the Docker Image

To build the Docker image for development, navigate to the directory containing the `Dockerfile.dev` and run the following command:

```bash
docker build -t mlops_course_dev:latest -f Dockerfile.dev .
```
This command builds and tags the resulting image with `mlops_course_dev:latest`

### Running the Docker Container
Once the image is built, you can run the Docker container using the following command:
```bash
docker run -it --name mlops_dev_feature --gpus all mlops_course_dev:latest
```
This command starts a container named `mlops_dev_feature` with GPU support enabled, allowing you to utilize the CUDA capabilities.

### Package Management Inside the Container
The container uses Miniconda as the primary package manager installed in the base environment. While you can use both Conda and Mamba for package management, it is recommended to use Mamba for installing and updating packages due to its faster dependency resolution capabilities. Here’s how you can install a package using Mamba:

```bash
mamba install <package_name>
```
Mamba is pre-installed and configured to work seamlessly with the existing Conda setup, ensuring efficient management of your Python packages.
