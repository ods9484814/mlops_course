set -a; source .env; set +a && \
kaggle datasets download -d kritanjalijain/amazon-reviews -p data/ && \
unzip data/amazon-reviews.zip -d data/ && \
rm data/amazon-reviews.zip
