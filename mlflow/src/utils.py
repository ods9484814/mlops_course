import re
import os

import nltk
import numpy as np
import polars as pl
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.metrics import ConfusionMatrixDisplay


nltk_data_path = os.path.expanduser("../nltk_data")
nltk.data.path.append(nltk_data_path)
nltk.download("stopwords", download_dir=nltk_data_path, quiet=True)
nltk.download("wordnet", download_dir=nltk_data_path, quiet=True)


def preprocessing(input_text: str) -> str:
    """
    Preprocesses the input text by converting it to lowercase, removing URLs, special characters, digits, and stopwords.

    Args:
        input_text (str): The text to be preprocessed.

    Returns:
        str: The cleaned and preprocessed text.
    """
    text = input_text.lower()
    text = re.sub(r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*", "", text)
    text = re.sub("[0-9 \-_]+", " ", text)
    text = re.sub("[^a-z A-Z]+", " ", text)
    text = " ".join(
        [word for word in text.split() if word not in stopwords.words("english")]
    )
    return text.strip()


def lemmatize(input_frame: pl.DataFrame) -> pl.DataFrame:
    """
    Lemmatizes the tokens in the 'corpus' column of the input DataFrame.

    Args:
        input_frame (pl.DataFrame): A Polars DataFrame with a 'corpus' column containing lists of tokens.

    Returns:
        pl.DataFrame: A new DataFrame with the lemmatized tokens in the 'corpus' column.
    """
    lemmatizer = WordNetLemmatizer()

    return input_frame.with_columns(
        pl.col("corpus").map_elements(
            lambda input_list: [lemmatizer.lemmatize(token) for token in input_list]
        )
    )


def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    """
    Plots a confusion matrix using the true and predicted labels.

    Args:
        y_true (np.ndarray): The ground truth labels.
        pred (np.ndarray): The predicted labels.

    Returns:
        Figure: A Matplotlib figure containing the confusion matrix plot.
    """
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(y_true, pred, ax=ax, colorbar=False)
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title("Confusion Matrix")
    plt.tight_layout()
    return fig
