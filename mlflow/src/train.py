import argparse
from pathlib import Path

import mlflow
import polars as pl
import torch

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader
from tqdm import tqdm
from transformers import AutoModel, AutoTokenizer

from utils import conf_matrix, lemmatize, preprocessing


def parse_arguments():
    """
    Parses command-line arguments for the Amazon reviews dataset and model type.

    Returns:
        argparse.Namespace: Parsed arguments including dataset path, model type, and experiment name.
    """
    parser = argparse.ArgumentParser(
        description="Parse arguments for Amazon reviews dataset and model type."
    )
    parser.add_argument(
        "--dataset_path", type=str, help="Path to the root dataset of Amazon reviews."
    )
    parser.add_argument(
        "--model_type",
        type=str,
        choices=["tfidf", "bert"],
        help="Type of model to use (tfidf or bert).",
    )
    parser.add_argument("--experiment_name", type=str, help="Name of the experiment.")
    return parser.parse_args()


def train_tfidf(data_frame, vectorizer_params, model_params, exp_name, random_state=42):
    """
    Trains a logistic regression model using TF-IDF features on the Amazon reviews dataset.

    Args:
        data_frame (pl.DataFrame): DataFrame containing the reviews and their polarities.
        vectorizer_params (dict): Parameters for the TfidfVectorizer.
        model_params (dict): Parameters for the LogisticRegression model.
        exp_name (str): Name of the MLflow experiment.
        random_state (int, optional): Random state for reproducibility. Defaults to 42.
    """
    progress_bar = tqdm(total=len(data_frame))

    processed_reviews = []
    for review in data_frame["Review"]:
        processed_reviews.append(preprocessing(review))
        progress_bar.update(1)

    progress_bar.close()

    data_frame = data_frame.with_columns(
        pl.Series(processed_reviews).str.split(" ").alias("corpus")
    )

    data_frame = lemmatize(data_frame)

    train, test = train_test_split(
        data_frame,
        test_size=0.3,
        shuffle=True,
        random_state=random_state,
    )

    tfidf_vectorizer = TfidfVectorizer(**vectorizer_params)
    tfidf_vectorizer.fit(train["corpus"].to_pandas().astype(str))
    train_features = tfidf_vectorizer.transform(train["corpus"].to_pandas().astype(str))
    test_features = tfidf_vectorizer.transform(test["corpus"].to_pandas().astype(str))
    model_lr = LogisticRegression(**model_params)
    model_lr.fit(train_features, train["Polarity"])
    predicts = model_lr.predict(test_features)
    report = classification_report(test["Polarity"], predicts, output_dict=True)

    mlflow.log_metric("accuracy", report.pop("accuracy"))
    for class_or_avg, metrics_dict in report.items():
        if class_or_avg == "macro avg":
            break
        for metric, value in metrics_dict.items():
            mlflow.log_metric(class_or_avg + "_" + metric, value)

    mlflow.log_params(vectorizer_params)
    mlflow.log_params(model_params)

    mlflow.sklearn.log_model(
        sk_model=model_lr,
        input_example=test_features[:10],
        artifact_path=f"mlflow/{exp_name}/model",
    )

    fig = conf_matrix(test["Polarity"], predicts)
    mlflow.log_figure(fig, f"{exp_name}_confusion_matrix.png")


def train_bert(
    data_frame: pl.DataFrame,
    model_type: str,
    model_params: dict,
    exp_name: str,
    random_state: int = 42,
    batch_size: int = 128,
):
    """
    Trains a logistic regression model using BERT embeddings on the Amazon reviews dataset.

    Args:
        data_frame (pl.DataFrame): DataFrame containing the reviews and their polarities.
        model_type (str): Type of BERT model to use.
        model_params (dict): Parameters for the LogisticRegression model.
        exp_name (str): Name of the MLflow experiment.
        random_state (int, optional): Random state for reproducibility. Defaults to 42.
        batch_size (int, optional): Batch size for DataLoader. Defaults to 128.
    """
    train, test = train_test_split(
        data_frame,
        test_size=0.3,
        shuffle=True,
        random_state=random_state,
    )
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    tokenizer = AutoTokenizer.from_pretrained(model_type)
    bert_model = AutoModel.from_pretrained(model_type).to(device)
    train_dataloader = DataLoader(
        train["Review"].to_list(), batch_size=batch_size, shuffle=False
    )
    test_dataloader = DataLoader(
        test["Review"].to_list(), batch_size=batch_size, shuffle=False
    )

    train_embeddings = []

    for batch_data in tqdm(train_dataloader, total=len(train_dataloader)):
        tokenized_batch = tokenizer(
            batch_data, padding=True, truncation=True, return_tensors="pt"
        ).to(device)

        with torch.no_grad():
            hidden_batch = bert_model(**tokenized_batch)
            batch_embeddings = (
                hidden_batch.last_hidden_state[:, 0, :].detach().to("cpu")
            )
            train_embeddings.append(batch_embeddings)

    train_embeddings = torch.concat(train_embeddings, dim=0)

    test_embeddings = []

    for batch_data in tqdm(test_dataloader, total=len(test_dataloader)):
        tokenized_batch = tokenizer(
            batch_data, padding=True, truncation=True, return_tensors="pt"
        ).to(device)

        with torch.no_grad():
            hidden_batch = bert_model(**tokenized_batch)
            batch_embeddings = (
                hidden_batch.last_hidden_state[:, 0, :].detach().to("cpu")
            )
            test_embeddings.append(batch_embeddings)

    test_embeddings = torch.concat(test_embeddings, dim=0)

    model_lr = LogisticRegression(**model_params)
    model_lr.fit(train_embeddings, train["Polarity"])
    predicts = model_lr.predict(test_embeddings)
    report = classification_report(test["Polarity"], predicts, output_dict=True)

    mlflow.log_metric("accuracy", report.pop("accuracy"))
    for class_or_avg, metrics_dict in report.items():
        if class_or_avg == "macro avg":
            break
        for metric, value in metrics_dict.items():
            mlflow.log_metric(class_or_avg + "_" + metric, value)

    mlflow.log_params(model_params)

    mlflow.sklearn.log_model(
        sk_model=model_lr,
        input_example=test_embeddings[:10].numpy(),
        artifact_path=f"mlflow/{exp_name}/model",
    )

    fig = conf_matrix(test["Polarity"], predicts)

    mlflow.log_figure(fig, f"{exp_name}_confusion_matrix.png")


if __name__ == "__main__":
    args = parse_arguments()

    amazon_root = Path(args.dataset_path)
    df = pl.read_csv(amazon_root / "train.csv", n_rows=100_000)
    df.columns = ["Polarity", "Title", "Review"]
    df = df.select("Polarity", "Review").with_columns(
        pl.col("Polarity").map_elements(
            lambda polarity: "Negative" if polarity == 1 else "Positive"
        )
    )

    mlflow.set_tracking_uri("http://127.0.0.1:8080")
    embeddings_experiment = mlflow.set_experiment("Amazon reviews classification")

    with mlflow.start_run(run_name=args.experiment_name) as run:
        if args.model_type == "tfidf":
            random_state = 42
            vectorizer_params = {"max_features": 10000, "analyzer": "word"}
            model_params = {
                "multi_class": "multinomial",
                "solver": "saga",
                "random_state": random_state,
            }
            train_tfidf(
                data_frame=df,
                vectorizer_params=vectorizer_params,
                model_params=model_params,
                exp_name=args.experiment_name,
                random_state=random_state,
            )
        elif args.model_type == "bert":
            random_state = 42
            batch_size = 128
            model_type = "bert-base-uncased"
            model_params = {
                "multi_class": "multinomial",
                "solver": "saga",
                "random_state": random_state,
            }
            train_bert(
                data_frame=df,
                model_type=model_type,
                model_params=model_params,
                exp_name=args.experiment_name,
                random_state=random_state,
                batch_size=batch_size,
            )
