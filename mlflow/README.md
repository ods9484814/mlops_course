# Instructions for Running Amazon Reviews Classification Training with MLflow

This guide provides step-by-step instructions on how to set up and execute the training of an Amazon reviews classification model with MLFlow.

## Prerequisites
Before you start, ensure you have [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installed on your system.

## Installation

### Step 1: Create Your Environment
We've provided an `environment-mlflow.yml` file that specifies all the necessary dependencies. Create your Conda environment by running:

```bash
conda env create -f environment-mlflow.yml
```
This command creates a new Conda environment named `mlflow` and installs all specified dependencies.

Then activate the newly created environment:
```bash
conda activate mlflow
```

### Step 2: Configure Environment Variables
1. Copy the example environment file to create your own `.env` file:
    ```bash
    cp example.env .env
    ```

2. Open the `.env` file and insert your Kaggle credentials (KAGGLE_USERNAME and KAGGLE_KEY). You can obtain these credentials from your Kaggle account settings under the API section.

### Step 3: Install Dataset
Run the following script to install [Amazon Reviews Dataset](https://www.kaggle.com/datasets/kritanjalijain/amazon-reviews):
```bash
./download_data.sh
```

### Step 4: Start the MLflow Server
Next, you need to start the MLflow server to track your experiments and models. This server will provide a user interface for visualizing and managing your MLflow runs. To start the server, use the following command:

```bash
mlflow server --host 127.0.0.1 --port 8080
```

This will launch the MLflow server on your local machine, accessible at http://127.0.0.1:8080.

### Step 5: Run the Training Script
Now, you can run the training script to start training the Amazon reviews classification model. The training script `src/train.py` accepts several arguments:

- `--dataset_path`: Path to the root dataset of Amazon reviews.
- `--model_type`: Type of model to use (`tfidf` or `bert`).
- `--experiment_name`: Name of the experiment.

Here's an example of how to run the training script:
```bash
python src/train.py --dataset_path data \
                    --model_type bert \
                    --experiment_name some_exp_name
```
After running this command, you will be able to see the conducted experiment in the MLflow interface.

### Step 6 (Optional): Compare Metrics

Below are the results from the two experiments using BERT and TF-IDF embeddings (both trained on 100,000 rows):

| Metric                  | BERT Embeddings | TF-IDF Embeddings |
|-------------------------|-----------------|-------------------|
| Negative F1-score       | **0.8843**      | 0.8533            |
| Negative Precision      | 0.8790          | **0.8575**        |
| Negative Recall         | **0.8896**      | 0.8492            |
| Negative Support        | 14,547          | 14,547            |
| Positive F1-score       | **0.8897**      | 0.8632            |
| Positive Precision      | **0.8949**      | 0.8593            |
| Positive Recall         | 0.8847          | **0.8672**        |
| Positive Support        | 15,453          | 15,453            |
| Accuracy                | **0.8871**      | 0.8585            |

Based on the accuracy metric, we select the **BERT** model (`bert_embeddings` experiment), as it demonstrates higher accuracy compared to the TF-IDF model. Given the relatively balanced class distribution in this dataset (15.4k/14.5k), accuracy is a suitable metric for evaluating the performance of our models.

To further illustrate the results, you can refer to the following artifacts and comparison screenshots:

- [BERT Overview](assets/bert_overview.png)
- [BERT Artifacts](assets/bert_artifacts.png)
- [TF-IDF Overview](assets/tfidf_overview.png)
- [TF-IDF Artifacts](assets/tfidf_artifacts.png)
- [Comparison](assets/compare.png) (This screenshot shows the two experiments sorted by the accuracy column in descending order)
