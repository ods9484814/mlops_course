import logging

import hydra

from dotenv import load_dotenv
from hydra.utils import instantiate
from loguru import logger
from omegaconf import DictConfig, OmegaConf
from utils import preprocess_data, save_artifacts


@hydra.main(
    config_path="../../configs",
    config_name=snakemake.params[0],
    version_base=None,  # noqa: F821
)
def main(cfg: DictConfig):
    std_logger = logging.getLogger(__name__)

    def loguru_to_standard_logging_handler(record):
        text = record.record["message"]
        level = record.record["level"].no
        std_logger.log(level, text)

    logger.add(loguru_to_standard_logging_handler, serialize=True)
    logger.info("Configuration and logger initialization complete.")

    OmegaConf.set_struct(cfg, False)
    model_name = cfg.model.pop("name")
    metric_name = cfg.metric.pop("name")
    dataset_name = cfg.dataset.pop("name")
    OmegaConf.set_struct(cfg, True)

    logger.debug(f"Model name extracted: {model_name}")
    logger.debug(f"Metric name extracted: {metric_name}")
    logger.debug(f"Dataset name extracted: {dataset_name}")

    model = instantiate(cfg.model)
    metric = instantiate(cfg.metric)
    logger.info("Model and metric instances created successfully.")

    (X_train, X_test, y_train, y_test) = preprocess_data(**cfg.dataset)
    logger.info("Data preprocessing completed.")

    model.fit(X_train, y_train)
    logger.info(f"Model training completed for model: {model_name}.")

    y_pred = model.predict(X_test)
    logger.info("Model predictions completed.")

    metrics = {metric_name: metric.compute(y_test, y_pred)}
    logger.info(f"Metrics computed: {metrics}")

    save_artifacts(model_name, model, metrics, cfg.artifacts)
    logger.info(f"Artifacts for {model_name} saved successfully.")


if __name__ == "__main__":
    load_dotenv()
    main()
