import os

from typing import Any, Dict, Tuple, List

import pandas as pd

from joblib import dump
from loguru import logger
from sklearn.metrics import f1_score, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


def preprocess_data(
    path: str,
    features: List[str],
    target: str,
    test_size: float = 0.35,
    random_state: int = 101,
) -> Tuple[pd.DataFrame, pd.DataFrame, pd.Series, pd.Series]:
    """
    Preprocesses the data from a CSV file by encoding the target variable and splitting the dataset into training and testing sets.

    Args:
        path (str): The file path to the CSV data file.
        features (List[str]): A list of column names to be used as features.
        target (str): The name of the column to be used as the target variable.
        test_size (float): The proportion of the dataset to include in the test split. Default is 0.35.
        random_state (int): The seed used by the random number generator. Default is 101.

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame, pd.Series, pd.Series]: A tuple containing the training and testing splits of the features and target. Specifically, it returns (x_train, x_test, y_train, y_test), where `x_train` and `x_test` are DataFrames containing the features for the training and testing sets, respectively, and `y_train` and `y_test` are Series containing the targets for the training and testing sets, respectively.
    """

    data = pd.read_csv(path)
    x = data[features]
    y = data[target]
    encoder = LabelEncoder()
    y = encoder.fit_transform(y)

    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=test_size, random_state=random_state
    )
    return (x_train, x_test, y_train, y_test)


def save_artifacts(
    model: str, model_obj: Any, metrics: Dict[str, float], artifact_dir: str
) -> None:
    """
    Saves the provided model object, metrics, and other model artifacts into specified directories.

    This function uses `joblib` to serialize and save the model object. It also saves each metric
    in a separate `.txt` file named after the metric. The function assumes the directories already exist.

    Args:
        model (str): The name of the model, e.g., 'randforest', 'logreg'.
        model_obj (any): The model object to be saved.
        metrics (dict): A dictionary containing the metric names as keys and their corresponding values.
        artifact_dir (str): The directory path where the model and other artifacts will be saved.

    Returns:
        None
    """

    if not os.path.exists(artifact_dir):
        logger.info(
            f"The artifact directory {artifact_dir} does not exist. Creating..."
        )
        os.makedirs(artifact_dir)

    for metric_name, metric_value in metrics.items():
        metric_path = os.path.join(artifact_dir, "metrics.txt")
        with open(metric_path, "w") as file:
            file.write(f"{metric_name}: {metric_value}\n")
        logger.info(f"Saved metric '{metric_name}' to {metric_path}")

    model_path = os.path.join(artifact_dir, f"{model}.joblib")
    dump(model_obj, model_path)
    logger.info(f"Model '{model}' saved successfully to {model_path}")

    logger.debug("Model, metrics, and other artifacts have been successfully saved.")


class F1Metric:
    """A wrapper class for the f1_score function from sklearn.metrics.

    This class stores the parameters for the f1_score function and provides
    a method to compute the F1 score using these parameters. It allows the
    flexibility to adjust the parameters via configuration files when used
    with Hydra.

    Attributes:
        params (dict): A dictionary of parameters that will be passed to the
            f1_score function. Typical parameters could include `labels`, `pos_label`,
            `average`, `sample_weight`, and `zero_division`.

    Methods:
        compute(y_true, y_pred): Computes the F1 score using the stored parameters.

    Examples:
        >>> f1_metric = F1Metric(pos_label=1, average='macro', zero_division='warn')
        >>> y_true = [0, 1, 2, 0, 1]
        >>> y_pred = [0, 2, 1, 0, 0]
        >>> f1_metric.compute(y_true, y_pred)
        0.333...  # Example output
    """

    def __init__(self, **kwargs):
        """Initializes F1Metric with given keyword arguments for parameters.

        Args:
            **kwargs: Arbitrary keyword arguments. These arguments are passed
                to the f1_score function. Common arguments include:
                - labels (list, optional): A list of label values that appear in y_true.
                - pos_label (int or str, optional): The label of the positive class.
                - average (str, optional): Required for multiclass/multilabel targets.
                    Could be 'micro', 'macro', 'samples', 'weighted', or None.
                - sample_weight (array-like, optional): Sample weights.
                - zero_division (str or int, optional): Controls handling of the zero division.
        """
        self.params = kwargs

    def compute(self, y_true, y_pred):
        """Computes the F1 score using the initialized parameters.

        Args:
            y_true (array-like): True labels of data.
            y_pred (array-like): Predicted labels, as returned by a classifier.

        Returns:
            float: The computed F1 score.
        """
        return f1_score(y_true, y_pred, **self.params)


class AccuracyMetric:
    """A wrapper class for the accuracy_score function from sklearn.metrics.

    This class stores the parameters for the accuracy_score function and provides
    a method to compute the accuracy using these parameters. It is designed to
    be flexible, allowing for parameter adjustments via configuration files when
    used with Hydra.

    Attributes:
        params (dict): A dictionary of parameters that will be passed to the
            accuracy_score function. Typical parameters might include `normalize`
            and `sample_weight`.

    Methods:
        compute(y_true, y_pred): Computes the accuracy using the stored parameters.

    Examples:
        >>> accuracy_metric = AccuracyMetric(normalize=True)
        >>> y_true = [0, 1, 2, 2, 1]
        >>> y_pred = [0, 2, 1, 2, 1]
        >>> accuracy_metric.compute(y_true, y_pred)
        0.6  # Example output
    """

    def __init__(self, **kwargs):
        """Initializes AccuracyMetric with given keyword arguments for parameters.

        Args:
            **kwargs: Arbitrary keyword arguments. These arguments are passed
                to the accuracy_score function. Common arguments include:
                - normalize (bool, optional): If `False`, return the number of correctly
                  classified samples. Otherwise, return the fraction of correctly
                  classified samples.
                - sample_weight (array-like, optional): Sample weights.
        """
        self.params = kwargs

    def compute(self, y_true, y_pred):
        """Computes the accuracy using the initialized parameters.

        Args:
            y_true (array-like): True labels of data.
            y_pred (array-like): Predicted labels, as returned by a classifier.

        Returns:
            float: The computed accuracy.
        """
        return accuracy_score(y_true, y_pred, **self.params)
