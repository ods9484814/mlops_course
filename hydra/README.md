# Working with Snakemake + Hydra

This README provides instructions for setting up your environment to use Snakemak and Hydra, including the installation of Apptainer, Snakemake and Hydra itself.

## Prerequisites

Before you start, ensure you have [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installed on your system. Miniconda will be used to install Snakemake.

## Installation

### Step 1: Install Apptainer

First, install Apptainer by following the instructions available on the official Apptainer website. You can find the installation guide [here](https://apptainer.org/docs/admin/main/installation.html#install-from-pre-built-packages)

### Step 2: Install Snakemake and Hydra

After installing Apptainer, proceed with installing Snakemake:

1. Install Mamba on the base environment:
   ```bash
   conda install -n base -c conda-forge mamba
   ```
2. Create a new conda environment named `snakemake` and install Snakemake in it:
    ```bash
    mamba create -c conda-forge -c bioconda -n snakemake snakemake
    ```
3. Activate the newly created `snakemake` environment:
    ```bash
    conda activate snakemake
    ```
4. Install `hydra-core` library (this is necessary for Snakemake to correctly launch the Hydra config through the Compose-API):
    ```bash
    pip install hydra-core
    ```

### Step 3: Configure Environment Variables
1. Copy the example environment file to create your own `.env` file:
    ```bash
    cp example.env .env
    ```
2. Open the `.env` file and insert your Kaggle credentials (KAGGLE_USERNAME and KAGGLE_KEY). You can obtain these credentials from your Kaggle account settings under the API section.
3. Optionally, you can open the Snakemake file and manually change the number of cores in the TOTAL_CORES variable. By default, it is set to 8 cores.

### Step 4: Run the Workflow
Execute the Snakemake workflow with Hydra using the following command. Replace `N` with the number of cores you wish to allocate for the process:
```bash
snakemake --cores N --use-singularity
```
This command will run the entire process inside an Apptainer container, leveraging the specified number of cores to execute the workflow efficiently.

### Step 5: Review Workflow Outputs
Upon the successful execution of the DAG, the following artifacts will be generated:
 - **`aggregate_report.csv`**: This is the primary output file, containing comprehensive metrics for each experiment (config file) trained on the [Iris Dataset](https://www.kaggle.com/datasets/uciml/iris)

All experiments will be stored in the experiments folder, along with all artifacts such as models, metrics, and logs. All logging (standard and loguru) is redirected to Hydra logs, so you can view all log entries within a single file created by Hydra.

Here is the DAG visualization:
![dag](assets/dag.svg)

### Step 6 (Optional): Reference the Reproduction Table
Below is a table that you can refer for reproducing the results:

| Config                      | Metric   | Value   |
|-----------------------------|----------|---------|
| **RandomForest (baseline)** | accuracy | `0.9623`|
| **Logistic Regression**     | F1       | `0.9811`|
| **K-Nearest Neighbors**     | F1       | `1.0`   |
