import sys

import pandas as pd

from sklearn.metrics import accuracy_score, f1_score


def calculate_metrics(predictions_path, truth_path, metric_name, output_path):
    """
    Calculates a specified metric based on predictions and true labels and saves the result to a CSV file.

    Args:
        predictions_path (str): The path to the CSV file containing the model predictions.
        truth_path (str): The path to the CSV file containing the true labels.
        metric_name (str): The name of the metric to calculate.
        output_path (str): The path where the metric result should be saved.

    Returns:
        None
    """

    predictions = pd.read_csv(predictions_path)["predictions"]
    target = pd.read_csv(truth_path)["Species"]

    if metric_name == "accuracy":
        result = accuracy_score(target, predictions)
    elif metric_name == "f1":
        result = f1_score(target, predictions, average="macro")
    else:
        raise ValueError(f"Unsupported metric: {metric_name}")

    with open(output_path, "w") as file:
        file.write(f"{round(result, 4)}\n")


if __name__ == "__main__":
    predictions_path = sys.argv[1]
    truth_path = sys.argv[2]
    metric_name = sys.argv[3]
    output_path = sys.argv[4]
    calculate_metrics(predictions_path, truth_path, metric_name, output_path)
