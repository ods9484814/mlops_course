import random
import sys

import joblib
import numpy as np
import pandas as pd

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.utils import check_random_state
from xgboost import XGBClassifier


def set_seed(seed=101):
    """
    Sets seed for reproducibility.

    This function sets the random seed for numpy, Python's random module, scikit-learn,
    and XGBoost to ensure deterministic behavior across multiple runs.

    Args:
        seed (int): The seed value to use for all random number generators. Default is 101.

    Returns:
        None
    """
    random.seed(seed)
    np.random.seed(seed)
    check_random_state(seed)


def train_model(input_path, output_path, model_type):
    """
    Trains a machine learning model and saves it to a file.

    Args:
        input_path (str): The path to the CSV file containing the training data.
        output_path (str): The path where the trained model should be saved.
        model_type (str): The type of model to train. Options are "logreg", "randforest", "knn", "xgboost".

    Returns:
        None
    """
    data = pd.read_csv(input_path)
    X = data.drop("Species", axis=1)
    y = data["Species"]

    model_type = model_type.lower()
    if model_type == "logreg":
        model = LogisticRegression()
    elif model_type == "randforest":
        model = RandomForestClassifier(max_depth=3)
    elif model_type == "knn":
        model = KNeighborsClassifier(n_neighbors=3)
    elif model_type == "xgboost":
        model = XGBClassifier(random_state=101)
    else:
        raise ValueError(f"Unsupported model type: {model_type}")

    model.fit(X, y)

    joblib.dump(model, output_path)


if __name__ == "__main__":
    input_path = sys.argv[1]
    output_path = sys.argv[2]
    model_type = sys.argv[3]
    set_seed()
    train_model(input_path, output_path, model_type)
