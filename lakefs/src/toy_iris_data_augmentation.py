import pandas as pd
import sys


def modify_iris_data(input_path):
    """
    Modifies the original Iris CSV data file by double the values of the 'PetalWidthCm' column.

    Args:
        input_path (str): The path to the original CSV data file. This path should be complete, including the filename.

    Returns:
        str: The path to the modified data file, which is saved in the same location as the original file,
        but with 'modified_' prefixed to the filename.
    """
    data = pd.read_csv(input_path)

    data["PetalWidthCm"] = data["PetalWidthCm"] * 2

    path_parts = input_path.split("/")
    filename = path_parts[-1]
    modified_filename = "modified_" + filename
    path_parts[-1] = modified_filename
    output_path = "/".join(path_parts)

    data.to_csv(output_path, index=False)
    return output_path


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python toy_iris_data_augmentation.py <path_to_iris_csv>")
        sys.exit(1)

    input_data_path = sys.argv[1]
    output_data_path = modify_iris_data(input_data_path)
    print(f"Modified data saved to: {output_data_path}")
