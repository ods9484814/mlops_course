# Working with LakeFS

This README provides instructions for setting up the environment, installing Snakemake, and deploying LakeFS over MinIO.

## Prerequisites

Before you start, ensure you have [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installed on your system. Miniconda will be used to install Snakemake.

Also you'll need to have Docker and Docker Compose installed. These tools are essential for creating and managing containers where your applications can run in a defined and replicable environment.
Installing Docker: Please follow the official installation guide for Docker (as example on Ubuntu) by visiting [Docker Installation for Ubuntu](https://docs.docker.com/engine/install/ubuntu/).
Installing Docker Compose: After installing Docker, you'll need Docker Compose to define and run multi-container Docker applications. For installation instructions, refer to the [Docker Compose on Ubuntu guide](https://docs.docker.com/compose/install/).

### Step 1: Setup Conda Environment

After installing prerequisites, proceed with installing Snakemake:

1. Install Mamba on the base environment:
   ```bash
   conda install -n base -c conda-forge mamba
   ```
2. Setup Conda Environment:
    We've provided an `environment-lakefs.yml` file that specifies all the necessary dependencies. Create your Conda environment by running:
   ```bash
   mamba env create -f environment-lakefs.yml
   ```
3. Activate the newly created environment:
    ```bash
    conda activate lakefs
    ```

### Step 2: Configure Environment Variables
1. Copy the example environment file to create your own `.env` file:
    ```bash
    cp example.env .env
    ```

2. Open the `.env` file and insert your Kaggle credentials (KAGGLE_USERNAME and KAGGLE_KEY). You can obtain these credentials from your Kaggle account settings under the API section.

### Step 3: Run the LakeFS Service
To initiate the LakeFS service using MinIO as the underlying storage layer, you'll use Docker Compose to build and start the containers:

```bash
docker-compose up
```

This command builds the Docker images if they don't exist or if they need to be rebuilt, and then starts the containers specified in `docker-compose.yml` file.

### Step 4: Run the Workflow
Execute the Snakemake workflow using the following command. Replace `N` with the number of cores you wish to allocate for the process:
```bash
snakemake --cores N
```
This command executes the entire workflow on the host machine, leveraging the specified number of cores to execute the workflow efficiently.

### Step 5: Review Workflow Outputs
Upon the successful execution of the DAG, the following artifacts will be generated:
 - **`aggregate_report.csv`**: This is the primary output file, containing comprehensive metrics for each model trained on the [Iris Dataset](https://www.kaggle.com/datasets/uciml/iris)

Additionally, the workflow produces several directories with more detailed outputs:
 - `data/`: contains all information about the datasets including the original, train, and test versions for each branch. The original dataset is uploaded to the `main` branch as part of the onstart rule. Additionally, a modified version of the dataset where the values of the 'PetalWidthCm' column are doubled is uploaded to the branch named `modified-data-20240512`. Subsequently, these datasets are downloaded and the branch names are referenced in all subsequent files to ensure that data management and tracking are streamlined and accurate.
 - `final/models/`: stores all the model weights that were trained
 - `final/metrics/`: includes separate files for each model's performance metrics, detailing accuracy comparisons
 - `final/inference/`: contains inference results for each model

Here is the DAG visualization:
![dag](assets/dag.svg)

### Step 6 (Optional): Reference the Reproduction Table
Below is a table that you can refer for reproducing the results:

| Dataset | Branch                 | Model      | Metric   | Value  |
|---------|------------------------|------------|----------|--------|
| iris    | main                   | logreg     | accuracy | 0.9811 |
|         |                        |            | f1       | 0.9811 |
|         |                        | randforest | accuracy | 0.9623 |
|         |                        |            | f1       | 0.9617 |
|         |                        | knn        | accuracy | 1.0    |
|         |                        |            | f1       | 1.0    |
|         |                        | xgboost    | accuracy | 0.9623 |
|         |                        |            | f1       | 0.9633 |
|         | modified-data-20240512 | logreg     | accuracy | 0.9811 |
|         |                        |            | f1       | 0.9811 |
|         |                        | randforest | accuracy | 0.9623 |
|         |                        |            | f1       | 0.9617 |
|         |                        | knn        | accuracy | 0.9811 |
|         |                        |            | f1       | 0.9811 |
|         |                        | xgboost    | accuracy | 0.9623 |
|         |                        |            | f1       | 0.9633 |
