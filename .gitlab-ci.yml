stages:
  - docker-ci
  - docker-snake
  - docker-hydra
  - linting
  - build
  - pages

services:
   - docker:24.0.5-dind

variables:
  DOCKER_TLS_CERTDIR: "/certs"
  IMAGE_NAME: ci

build-dev-image:
  image: docker:24.0.5
  stage: docker-ci
  rules:
    - changes:
        - Dockerfile.dev
        - .gitlab-ci.yml
        - environment-dev.yml
        - pyproject.toml
  before_script:
    - docker info
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker build -t $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$IMAGE_NAME:latest -f Dockerfile.dev .
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$IMAGE_NAME:latest

default:
  image: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$IMAGE_NAME:latest

linting-running:
  stage: linting
  script:
    - black .
    - ruff check .

build-release:
  stage: build
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9]+(\.[0-9]+)*$/'
  script:
    - echo "Bulding Python package..."
    - poetry build
    - echo "Build done!"
    - echo "Publishing package to gitlab package registry..."
    - poetry config repositories.gitlab "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi"
    - poetry config http-basic.gitlab gitlab-ci-token "$CI_JOB_TOKEN"
    - poetry publish --repository gitlab
    - echo "Done!"
  artifacts:
    expire_in: never
    paths:
      - $CI_PROJECT_DIR/dist

pages:
  stage: pages
  before_script:
    - pip install kaggle==1.6.12
    - mkdir -p ~/.kaggle && mkdir /datasets
    - echo "{\"username\":\"$KAGGLE_USERNAME\",\"key\":\"$KAGGLE_KEY\"}" > ~/.kaggle/kaggle.json
    - chmod 600 ~/.kaggle/kaggle.json
    - kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data -p /datasets/
    - unzip /datasets/ny-2015-street-tree-census-tree-data.zip -d /datasets/ny-2015-street-tree-census-tree-data/
  script:
    - mamba run quarto render docs
    - cp -r docs/_site public
    - echo "Pages accessible through ${CI_PAGES_URL}"
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

build-snakemake-image:
  image: docker:24.0.5
  stage: docker-snake
  rules:
    - changes:
        - snakemake/Dockerfile
        - snakemake/environment-snakemake.yml
  before_script:
    - docker info
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - cd snakemake/
  script:
    - docker build -t $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/snakemake:latest .
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/snakemake:latest

build-hydra-image:
  image: docker:24.0.5
  stage: docker-hydra
  rules:
    - changes:
        - hydra/Dockerfile
        - hydra/environment-hydra.yml
  before_script:
    - docker info
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - cd hydra/
  script:
    - docker build -t $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/hydra:latest .
    - docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/hydra:latest
