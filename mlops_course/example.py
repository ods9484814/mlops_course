def fibonacci(n):
    """
    Calculate the nth Fibonacci number.

    Args:
    n (int): The position in the Fibonacci sequence (must be a non-negative integer).

    Returns:
    int: The nth Fibonacci number.
    """
    if n <= 0:
        raise ValueError("Input should be a positive integer")
    elif n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)
