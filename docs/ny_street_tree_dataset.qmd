---
title: "Quarto Basics"
format:
  html:
    code-fold: true
jupyter: python3
---

```{python}
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import geopandas as gpd
import contextily as ctx

pd.set_option('display.max_columns', None)
```

```{python}
df = pd.read_csv('/datasets/ny-2015-street-tree-census-tree-data/2015-street-tree-census-tree-data.csv')
```
```{python}
df.head()
```
```{python}
df.info()
```
```{python}
df.describe()
```
```{python}
df.describe(include=['object'])
```
```{python}
null_counts = df.isnull().sum()
print(null_counts[null_counts > 0])
```
## Visualization
### Simple visualizations
```{python}
#| label: fig-1
#| fig-cap: "Distribution of Tree Diameter at Breast Height (DBH)"
sns.histplot(df['tree_dbh'].clip(upper=50), bins=40, kde=True)

plt.title('Distribution of Tree Diameter at Breast Height (DBH)')
plt.xlabel('Diameter at Breast Height (inches)')
plt.ylabel('Frequency')

plt.show()
```
```{python}
#| label: fig-2
#| fig-cap: "Health of Trees"
sns.countplot(x='health', data=df)
plt.title('Health of Trees')
plt.xlabel('Health')
plt.ylabel('Count')
plt.show()
```
```{python}
#| label: fig-3
#| fig-cap: "Relationship Between Health and Stewardship"
ct = pd.crosstab(df['health'], df['steward'])
sns.heatmap(ct, annot=True, fmt="d", cmap="YlGnBu")
plt.title('Relationship Between Health and Stewardship')
plt.xlabel('Steward')
plt.ylabel('Health')
plt.show()
```
```{python}
#| label: fig-4
#| fig-cap: "Tree Health by Stewardship Levels"
sns.countplot(x='health', hue='steward', data=df)
plt.title('Tree Health by Stewardship Levels')
plt.xlabel('Health')
plt.ylabel('Count')
plt.legend(title='Stewardship')
plt.show()
```
```{python}
#| label: fig-5
#| #| fig-cap: "Top 10 Tree Problems"
problems = df['problems'].value_counts().head(10)
sns.barplot(x=problems.values, y=problems.index)
plt.title('Top 10 Tree Problems')
plt.xlabel('Count')
plt.ylabel('Problems')
plt.show()
```
```{python}
#| label: fig-6
#| fig-cap: "Number of Tree Plantings by Year"
df['created_at'] = pd.to_datetime(df['created_at'])
df['year'] = df['created_at'].dt.year
sns.countplot(x='year', data=df)
plt.title('Number of Tree Plantings by Year')
plt.xlabel('Year')
plt.ylabel('Count')
plt.xticks(rotation=45)
plt.show()
```
```{python}
#| label: fig-7
#| fig-cap: "Impact of Care Level on Tree Health"
ct_steward_health = pd.crosstab(df['steward'], df['health'])
sns.heatmap(ct_steward_health, annot=True, cmap="YlGnBu", fmt="d")
plt.title('Impact of Care Level on Tree Health')
plt.xlabel('Care Level')
plt.ylabel('Health')
plt.show()
```
### Recommended visualizations
```{python}
#| label: fig-8
#| fig-cap: "Pairwise distribution of the features of interest"
features_of_interest = ['tree_dbh', 'stump_diam', 'latitude', 'longitude']
numerical_features_of_interest = df.select_dtypes(include=[np.number])[features_of_interest]
sns.pairplot(numerical_features_of_interest, diag_kind='kde')
plt.show()
```
```{python}
#| label: fig-9
#| fig-cap: "Matrix of pairwise correlations between features"
correlation_matrix = df.select_dtypes(include=[np.number]).corr()
plt.figure(figsize=(12, 8))
sns.heatmap(correlation_matrix, annot=True, cmap="coolwarm", fmt=".2f")
plt.title('Correlation Matrix of Numeric Features')
plt.show()
```
#### Geographical visualization
```{python}
#| label: fig-10
#| fig-cap: "Trees visualization on the map"
gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.longitude, df.latitude))

gdf.set_crs(epsg=4326, inplace=True)
gdf = gdf.to_crs(epsg=3857)
fig, ax = plt.subplots(figsize=(10, 10))
gdf.plot(
    ax=ax, alpha=0.5, column='borough', legend=True,
    categorical=True, markersize=0.05)
ctx.add_basemap(ax, source=ctx.providers.CartoDB.Positron)
ax.set_axis_off()
plt.show()
```
