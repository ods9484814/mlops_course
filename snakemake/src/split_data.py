import sys

import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


def split_data(input_file, train_output, test_output, test_size):
    """
    Splits the dataset into training and testing sets.

    Args:
        input_file (str): The path to the input CSV file containing the dataset.
        train_output (str): The path where the training set CSV will be saved.
        test_output (str): The path where the testing set CSV will be saved.
        test_size (float): The proportion of the dataset to include in the test split.

    Returns:
        None
    """
    data = pd.read_csv(input_file)
    x = data[["SepalLengthCm", "SepalWidthCm", "PetalLengthCm", "PetalWidthCm"]]
    y = data["Species"]

    encoder = LabelEncoder()
    y = encoder.fit_transform(y)

    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=test_size, random_state=101
    )

    train_set = pd.concat(
        [x_train, pd.DataFrame(y_train, columns=["Species"], index=x_train.index)],
        axis=1,
    )
    test_set = pd.concat(
        [x_test, pd.DataFrame(y_test, columns=["Species"], index=x_test.index)], axis=1
    )
    train_set.to_csv(train_output, index=False)
    test_set.to_csv(test_output, index=False)


if __name__ == "__main__":
    input_file = sys.argv[1]
    train_output = sys.argv[2]
    test_output = sys.argv[3]
    test_size = float(sys.argv[4])

    split_data(input_file, train_output, test_output, test_size)
