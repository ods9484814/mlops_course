import sys

import joblib
import pandas as pd


def inference(model_path, data_path, output_path):
    """
    Inference model and writes the predictions to a CSV file.

    Args:
        model_path (str): The path to the serialized model file (joblib format).
        data_path (str): The path to the CSV file containing the test data.
        output_path (str): The path where the prediction results should be saved.

    Returns:
        None
    """
    model = joblib.load(model_path)
    data = pd.read_csv(data_path).drop("Species", axis=1)

    predictions = model.predict(data)

    pd.DataFrame(predictions, columns=["predictions"]).to_csv(output_path, index=False)


if __name__ == "__main__":
    model_path = sys.argv[1]
    data_path = sys.argv[2]
    output_path = sys.argv[3]
    inference(model_path, data_path, output_path)
