# Working with Snakemake

This README provides instructions for setting up your environment to use Snakemake, including the installation of Apptainer and Snakemake itself.

## Prerequisites

Before you start, ensure you have [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installed on your system. Miniconda will be used to install Snakemake.

## Installation

### Step 1: Install Apptainer

First, install Apptainer by following the instructions available on the official Apptainer website. You can find the installation guide [here](https://apptainer.org/docs/admin/main/installation.html#install-from-pre-built-packages)

### Step 2: Install Snakemake

After installing Apptainer, proceed with installing Snakemake:

1. Install Mamba on the base environment:
   ```bash
   conda install -n base -c conda-forge mamba
   ```
2. Create a new conda environment named `snakemake` and install Snakemake in it:
    ```bash
    mamba create -c conda-forge -c bioconda -n snakemake snakemake
    ```
3. Activate the newly created `snakemake` environment:
    ```bash
    conda activate snakemake
    ```

### Step 3: Configure Environment Variables
1. Copy the example environment file to create your own `.env` file:
    ```bash
    cp example.env .env
    ```

2. Open the `.env` file and insert your Kaggle credentials (KAGGLE_USERNAME and KAGGLE_KEY). You can obtain these credentials from your Kaggle account settings under the API section.

### Step 4: Run the Workflow
Execute the Snakemake workflow using the following command. Replace `N` with the number of cores you wish to allocate for the process:
```bash
snakemake --cores N --use-singularity
```
This command will run the entire process inside an Apptainer container, leveraging the specified number of cores to execute the workflow efficiently.

### Step 5: Review Workflow Outputs
Upon the successful execution of the DAG, the following artifacts will be generated:
 - **`aggregate_report.csv`**: This is the primary output file, containing comprehensive metrics for each model trained on the [Iris Dataset](https://www.kaggle.com/datasets/uciml/iris)

Additionally, the workflow produces several directories with more detailed outputs:
 - `datasets/`: contains all information about the dataset including the original, train, and test versions
 - `models/`: stores all the model weights that were trained
 - `metrics/`: includes separate files for each model's performance metrics, detailing accuracy comparisons
 - `inference/`: contains inference results for each model

Here is the DAG visualization:
![dag](assets/dag.svg)

### Step 6 (Optional): Reference the Reproduction Table
Below is a table that you can refer for reproducing the results:

| Model      | Metric   | Value  |
|------------|----------|--------|
| **Logistic Regression** | accuracy | `0.9811` |
|            | F1-score       | `0.9811` |
| **Random Forest**      | accuracy | `0.9623` |
|            | F1-score       | `0.9617` |
| **K-Nearest Neighbors**| accuracy | `1.0`    |
|            | F1-score       | `1.0`    |
| **XGBoost**            | accuracy | `0.9623` |
|            | F1-score       | `0.963`  |
