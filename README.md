# mlops_course

## Getting started
We use `Miniconda` to manage dependencies and ensure consistency across development environments. Follow these steps to set up your environment:

### 1. Install Miniconda

If you haven't already, download and install Miniconda from [Miniconda's official site](https://docs.conda.io/en/latest/miniconda.html).

### 2. Clone the Repository

Clone the repository:

```bash
git clone https://gitlab.com/ods9484814/mlops_course.git
cd mlops_course
```

### 3. Create Your Environment
We've provided an `environment.yml` file that specifies all the necessary dependencies. Create your Conda environment by running:

```bash
conda env create -f environment.yml
```
This command creates a new Conda environment named mlops_course and installs all specified dependencies.

### 4. Activate Your Environment
Activate the newly created environment:
```bash
conda activate mlops_course
```

## Experimentation Methodology

### `develop` Branch

- The `develop` branch serves as the primary integration branch for features.
- Features are only merged into `develop` if they contribute positively to the project's key performance metrics.
- Features that do not improve the key metrics are thoroughly documented and remain as feature branches.
- Regular commits to `develop` are encouraged to ensure it reflects the latest advancements in the project.

### `main` Branch

- The `main` branch represents the stable version of the project and is used for deployment purposes.
- It is updated by merging changes from the `develop` branch once they are deemed stable and production-ready.
- The `main` branch is the source of truth for the most recent, deployable state of the project.

## Image Management with DVC

- Data is managed outside the repository using Data Version Control (DVC), adhering to best practices in data versioning and workflow management for machine learning projects.
- DVC is used to track and manage data versions without storing the data within the Git repository, ensuring efficient handling of large datasets.
- Each data version used will be recorded in project reports to maintain transparency and reproducibility.


## (Optional) Docker Environment

This section provides instructions on how to build and run Docker image for the project.

### Building the Docker Image

To build the Docker image, navigate to the directory containing the `Dockerfile` and run the following command:

```bash
docker build -t mlops_course:latest .
```
This command builds and tags the resulting image with `mlops_course:latest`

### Running the Docker Container
Once the image is built, you can run the Docker container using the following command:
```bash
docker run -it --name mlops_course_container --gpus all mlops_course:latest
```
This command starts a container named `mlops_course_container` with GPU support enabled, allowing you to utilize the CUDA capabilities.

### Package Management Inside the Container
The container uses Miniconda as the primary package manager installed in the base environment. While you can use both Conda and Mamba for package management, it is recommended to use Mamba for installing and updating packages due to its faster dependency resolution capabilities. Here’s how you can install a package using Mamba:

```bash
mamba install <package_name>
```
Mamba is pre-installed and configured to work seamlessly with the existing Conda setup, ensuring efficient management of your Python packages.
